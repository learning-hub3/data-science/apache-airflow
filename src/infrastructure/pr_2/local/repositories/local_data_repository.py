import domain.pr_2.interfaces.repositories.source_data_repository_interface as source_interface
import domain.pr_2.models.data_source as data_source_model

from datetime import datetime
import os

class LocalDataRepository(source_interface.SourceDataRepositoryInterface):
    def __init__(self, base_dir: str):
        self._base_dir = base_dir


    def get_data(self, start_date: datetime, end_date: datetime) -> list[data_source_model.DataSource]:
        data = []
        for filename in os.listdir(self._base_dir):
            file_path = os.path.join(self._base_dir, filename)
            content = self._get_data(file_path)
            data.append(data_source_model.DataSource(filename, content))

        return data


    def _get_data(self, location: str) -> str:
        file = ""
        with open(location, 'r') as f:
            for line in f.readlines():
                file += line

        return file  
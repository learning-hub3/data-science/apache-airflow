import domain.pr_2.interfaces.repositories.cache_repository_interface as cache_interface
import domain.pr_2.models.data_source as data_source_model
import os, shutil

class LocalCache(cache_interface.CacheRepositoryInterface):

    def __init__(self, cache_base_dir: str):
        self._cache_base_dir = cache_base_dir
    
    def read_data(self, key:str) -> data_source_model.DataSource:
        file = ""
        with open(os.path.join(self._cache_base_dir, key), 'r') as f:
            for line in f.readlines():
                file += line
        
        return data_source_model.DataSource(key, file)
    
    def list_keys(self) -> list[str]:
        return os.listdir(self._cache_base_dir)
    
    def write_data(self, data_source: data_source_model.DataSource) -> None:
        with open(os.path.join(self._cache_base_dir, data_source.key), 'w') as f: 
            f.write(data_source.content)
    
    def clean_cache(self) -> None:
        for filename in self.list_keys():
            file_path = os.path.join(self._cache_base_dir, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))
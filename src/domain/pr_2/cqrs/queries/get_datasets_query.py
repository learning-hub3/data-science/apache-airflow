import domain.pr_2.interfaces.cqrs.queries.get_datasets_query_interface as query_interface
import domain.pr_2.interfaces.repositories.source_data_repository_interface as source_interface
import domain.pr_2.interfaces.repositories.cache_repository_interface as cache_interface

from datetime import datetime

class GetDatasetsQuery(query_interface.GetDatasetsQueryInterface):

    def __init__(self, data_source: source_interface.SourceDataRepositoryInterface, cache: cache_interface.CacheRepositoryInterface):
        self._data_source = data_source
        self._cache = cache

    def execute(self, start_date: datetime, end_date: datetime) -> None:
        datasets = self._data_source.get_data(start_date, end_date)
        for dataset in datasets:
            self._cache.write_data(dataset)

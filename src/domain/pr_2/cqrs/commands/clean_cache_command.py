import domain.pr_2.interfaces.cqrs.commands.clean_cache_command_interface as clean_cache_interface
import domain.pr_2.interfaces.repositories.cache_repository_interface as cache_interface

class CleanCacheCommand(clean_cache_interface.CleanCacheCommandInterface):

    def __init__(self, cache: cache_interface.CacheRepositoryInterface):
        self._cache = cache

    def execute(self) -> None:
        self._cache.clean_cache()
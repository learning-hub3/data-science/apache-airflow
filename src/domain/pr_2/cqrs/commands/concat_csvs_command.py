import domain.pr_2.interfaces.cqrs.commands.concat_csvs_command_interface as concat_csvs_interface
import domain.pr_2.interfaces.repositories.cache_repository_interface as cache_interface
import domain.pr_2.models.data_source as datasource_model
from io import StringIO
import pandas as pd

class ConcatCsvsInterface(concat_csvs_interface.ConcatCsvsCommandInterface):
    def __init__(self, cache: cache_interface.CacheRepositoryInterface):
        self._cache = cache

    def execute(self, target_cache_key: str) -> None:
        data_frames = []
        keys = self._cache.list_keys()
        source_cache_keys = [x for x in keys if x.endswith(".csv")]
        for cache_entry in source_cache_keys:
            datasource = self._cache.read_data(cache_entry)
            content_as_string_io = StringIO(datasource.content)
            data_frame = pd.read_csv(content_as_string_io, index_col=False)
            data_frames.append(data_frame)
        merged_data_frame = pd.concat(data_frames)
        content = merged_data_frame.to_csv(index=False)
        datasource = datasource_model.DataSource(target_cache_key, content)
        self._cache.write_data(datasource)
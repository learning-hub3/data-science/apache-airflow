import domain.pr_2.interfaces.cqrs.commands.inner_join_command_interface as inner_join_interface
import domain.pr_2.interfaces.repositories.cache_repository_interface as cache_interface
import domain.pr_2.models.data_source as data_source_model
from io import StringIO
import pandas as pd


class InnerJoinCommand(inner_join_interface.InnerJoinCommandInterface):
    def __init__(self, cache: cache_interface.CacheRepositoryInterface):
        self._cache = cache

    def execute(self, target_cache_key: str, complete_base_data: str) -> None:
        keys = self._cache.list_keys()
        sources = [x for x in keys if x.endswith(".json")]
        complete_data_frame = self._get_data_frame(complete_base_data)

        for i in range(0, len(sources)):
            source = sources[i]
            data_frame = self._get_data_frame(source)
            complete_data_frame = pd.merge(complete_data_frame, data_frame, on="id", how="inner")

        content = complete_data_frame.to_csv(index=False)
        datasource = data_source_model.DataSource(target_cache_key, content)
        self._cache.write_data(datasource)

    def _get_data_frame(self, source: str):
        datasource = self._cache.read_data(source)
        content_as_io = StringIO(datasource.content)
        data_frame = pd.read_csv(content_as_io, index_col=False) if source.endswith(".csv") else pd.read_json(content_as_io)
        return data_frame
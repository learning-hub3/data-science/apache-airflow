import abc

import domain.models.employee as employee

class EmployeeRepositoryInterface(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def save(self, location: employee.Employee) -> None:
        return
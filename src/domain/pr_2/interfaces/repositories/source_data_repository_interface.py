import abc
from datetime import datetime

import domain.pr_2.models.data_source as data_source_model

class SourceDataRepositoryInterface(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def get_data(self, start_date: datetime, end_date: datetime) -> list[data_source_model.DataSource]:
        return
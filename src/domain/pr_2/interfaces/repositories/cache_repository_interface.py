import abc

import domain.pr_2.models.data_source as data_source_model

class CacheRepositoryInterface(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def read_data(self, key:str) -> data_source_model.DataSource:
        return
    
    @abc.abstractmethod
    def list_keys(self) -> list[str]:
        return
    
    @abc.abstractmethod
    def write_data(self, data_source: data_source_model.DataSource) -> None:
        return
    
    @abc.abstractmethod
    def clean_cache(self) -> None:
        return
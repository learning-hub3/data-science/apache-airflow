import abc

class InnerJoinCommandInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def execute(self, target_cache_key: str, complete_base_dataset: str) -> None:
        return
import abc

class SaveEmployeesCommandInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def execute(self, source_csv: str) -> None:
        return
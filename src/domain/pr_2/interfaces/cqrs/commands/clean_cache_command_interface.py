import abc

class CleanCacheCommandInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def execute(self) -> None:
        return
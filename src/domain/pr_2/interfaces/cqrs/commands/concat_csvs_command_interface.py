import abc

class ConcatCsvsCommandInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def execute(self, target_cache_key: str) -> None:
        return
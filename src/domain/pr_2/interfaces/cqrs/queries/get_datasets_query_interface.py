import abc
from datetime import datetime

class GetDatasetsQueryInterface(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def execute(self, start_date: datetime, end_date: datetime) -> None:
        return
class Employee:

    def __init__(self, id: int, name: str, wage: float, cpf: str):
        self.id = id
        self.name = name
        self.wage = wage
        self.cpf = cpf
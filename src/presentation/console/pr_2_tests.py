import sys
sys.path.append("/home/cicero/git/Graduacao Ciencia de Dados/Modulo 3B/ApacheAirflow/src")

import domain.pr_2.cqrs.queries.get_datasets_query as get_datasets_query
import infrastructure.pr_2.local.repositories.local_data_repository as local_data_repository
import infrastructure.pr_2.local.repositories.local_cache as local_cache
import domain.pr_2.cqrs.commands.concat_csvs_command as concat_csvs_command
import domain.pr_2.cqrs.commands.inner_join_command as inner_join_command
import domain.pr_2.cqrs.commands.clean_cache_command as clean_cache_command

from datetime import datetime

if(__name__ == "__main__"):
    clean_cache = True
    cache_base_dir = "/home/cicero/git/Graduacao Ciencia de Dados/Modulo 3B/ApacheAirflow/assets/tmp"
    dataset_base_dir = "/home/cicero/git/Graduacao Ciencia de Dados/Modulo 3B/ApacheAirflow/src/infrastructure/pr_2/local/files"
    dummy_datetime = datetime(2024,5,20)


    cache = local_cache.LocalCache(cache_base_dir)
    data_repository = local_data_repository.LocalDataRepository(dataset_base_dir)

    get_data_query = get_datasets_query.GetDatasetsQuery(data_repository, cache)
    get_data_query.execute(dummy_datetime, dummy_datetime)

    all_employees = "all_employees.csv"
    concat_command = concat_csvs_command.ConcatCsvsInterface(cache)
    concat_command.execute(all_employees)

    join_command = inner_join_command.InnerJoinCommand(cache)
    join_command.execute("all_employees_with_cpf.csv", all_employees)

    if(clean_cache):
        clean_comand = clean_cache_command.CleanCacheCommand(cache)
        clean_comand.execute()